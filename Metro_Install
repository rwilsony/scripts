#!/bin/bash

set -e
tabs 4

# NOW WITH AUTO COMPLETION
# To enable auto completion copy Metro_Install_completion_file into /etc/bash_completion.d/ then 'source' it or restart bash
#   sudo cp "$HOME"/scripts/Metro_Install_completion_file /etc/bash_completion.d/; source /etc/bash_completion.d/Metro_Install_completion_file

RED='\033[0;31m'
NC='\033[0m' # No Color

#Edit this path to point to the folder that contains metropolis_sw
HOME_DIR="/home/$USER"
if [ -z $USER ]; then
    HOME_DIR="/home/metro"
fi
WORKSPACE_DIR="${HOME_DIR}/workspace"
FIRST_SPACE="${WORKSPACE_DIR}/metropolis_sw"
OTHER_SPACE="${WORKSPACE_DIR}/metropolis_OTHER/metropolis_sw"
UBUNTU_SPACE="${WORKSPACE_DIR}/metropolis_ubuntu/metropolis_sw"
FRESH_SPACE="${WORKSPACE_DIR}/metropolis_freshStart/metropolis_sw"
THE_REAL_SLIM_SUDO=/usr/bin/sudo

INSTALL_DIR="/usr/local/metropolis"

PUSHBULLET_TOKEN="`cat ${HOME_DIR}/scripts/pushBulletToken`"
INSTALL=true
INSTALL_REMOTE=false
FORCE_INSTALL=false
BUILD=true
REFRESHUI=true
CLEANFIRST=false
MAKEDEPEND=false
BUILDRPM=false
BUILDTAR=false
BUILDUI=false
BUILDUIDEVEL=false
UIINSTALL=false # special flag used only when building the UI
MOBILEALERT=false # Flag for sending pushbullet notification
QUIET=false
HOMESPACE=false
BUILDSPECIFIC=false
BUILDSPECIFICTYPE="debug"
BUILDSPECIFICNAME=false
FORCE_REFRESH=false # Don't save the run and run_comp changes
PURGE_RUN=false
ENABLE_DEBUG_KEYS=true
MASTERBLASTER="Fr3der" # Sometimes it's nice for it to just install, no questions asked.
REMOTEUSER="metro"
REMOTESUPER="root"
REMOTE_TARGET=false
BUILDREMOTE=false
REMOTE_NOSTART=false
REMOTESTOPPED=false
FUN_MESSAGE=false
NUM_SPECIFIC_BUILD=0
REMOTE_REBOOT=false


# pushMyBullet takes the message to send as the first argument and sends the notification to pushBullet
#               Optionally it can take a second parameter to change the title of the message.
pushMyBullet()
{
    MESSAGE_TITLE="Message from `basename $0`"
    MESSAGE_BODY="$1"
    if [ ! -z "$2" ] ; then
        MESSAGE_TITLE=$2
    fi

    if [ "${MOBILEALERT}" = false ] ; then
        echo -e ${RED}"$MESSAGE_BODY"
    else
        echo "Sending \"$MESSAGE_BODY\" to Pushbullet"
        curl --silent -u """$PUSHBULLET_TOKEN"":" -d type="note" -d body="$MESSAGE_BODY" \
             -d title="$MESSAGE_TITLE" 'https://api.pushbullet.com/v2/pushes' &>/dev/null
    fi
}

tooLate()
{
    #If there are any backgrounded jobs (or any jobs left), kill them
    #No witnesses
    if [[ $(jobs -p) ]]; then
        kill $(jobs -p);
        sleep 1s;
        jobs;
    fi
}

printHelp()
{
    echo "--install                                 Just install"
    echo "--build                                   Just build"
    echo "--force_install                           Force update $INSTALL_DIR regardless of timestamps (Doesn't deactivate build)"
    echo "--depend(s)                               Also make depend first"
    echo "--clean|--spotless                        Clean before building"
    echo "--just_rpm                                Just build an rpm"
    echo "--rpm_install                             Just install the last built RPM to a remote host"
    echo "--rpm                                     Also build an rpm"
    echo "--ui                                      Just build and install the UI"
    echo "--uidevel                                 Just build and install the UI using devel environment"
    echo "--noUIRefresh                             After building just the UI, don't refresh."
    # echo "--quiet                                   Don't print stuff other than the spinners" # needs work
    echo "--notify                                  Send pushbullet notification on finish or error."
    echo "--just_notify                             Sometimes it's fun to send yourself a message."
    echo "--METRO                                   Build the stuff in $FIRST_SPACE"
    echo "--OTHER                                   Build the stuff in $OTHER_SPACE"
    echo "--UBUNTU                                  Build the stuff in $UBUNTU_SPACE"
    echo "--FRESH                                   Build the stuff in $FRESH_SPACE"
    echo "-h|--help                                 Print this stuff"
    echo "--no_run_reserve                          Don't save the run_comp changes"
    echo "--build_comp|--build_compositor           Build and copy over Compositor"
    echo "--build_script|--build_scriptApp          Build and copy over scriptApp"
    echo "--build_release|--buildRelease [thing]    Build as release and copy over specified 'thing', like 'tileLib'."
    echo "--build_debug|--buildDebug [thing]        Build as debug and copy over specified 'thing', like 'tileLib'."
    echo "--build_thing|--buildThing [thing]        Same as --build_debug."
    echo "--enable_debug_keys                       Also use sed to enable debug key bindings in $INSTALL_DIR/scripts/run_comp (currently stuck on by choice)"
    echo "--just_debug_keys                         Just use sed to enable debug key bindings in $INSTALL_DIR/scripts/run_comp (no build/install)"
    echo "--purge_run_scripts                       Deletes the run_comp script in $INSTALL_DIR and puts a new copy in."
    echo "--remote_target [target]                  Tries to install to target"
    echo "--remote_noStart [target]                 Won't start the target again (Useful for chaining.)"
    echo "--just_tar                                Builds a full install like an RPM, but in a tar.gz format."
}
# Just get the flags
# find      [a-z "#]+([-a-z_]+)\|?([-a-z_]*).+
# replace   $1 $2

# pushMyBullet sends a push bullet notification if --notify was used
# tooLate goes and kills any background jobs. Was useful when trying to do a --quiet build with a custom spinner.
trap "pushMyBullet \"THERE HAS BEEN AN ERROR RUNNING `basename $0`\"; tooLate;" ERR
# trap "pushMyBullet \"THERE HAS BEEN AN ERROR RUNNING `basename $0`\"; printHelp; tooLate;" ERR


POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    --METRO) # Specifies to use the metro
    HOMESPACE="${FIRST_SPACE}"
    shift # past argument
    ;;

    --OTHER) # Specifies to use the other workspace
    HOMESPACE="${OTHER_SPACE}"
    shift # past argument
    ;;

    --UBUNTU) # Specifies to use the newer workspace
    HOMESPACE="${UBUNTU_SPACE}"
    shift # past argument
    ;;

    --FRESH) # Specifies to use the fresh workspace
    HOMESPACE="${FRESH_SPACE}"
    shift # past argument
    ;;

    -h|--help) # print the help and quit
    printHelp
    exit 0
    ;;

    --install) # Just install
    BUILD=false
    INSTALL=true
    shift # past argument
    ;;

    --force_install) # Force $INSTALL_DIR update
    INSTALL=true
    FORCE_INSTALL=true
    shift # past argument
    ;;

    --build) # Just build
    INSTALL=false
    UIINSTALL=false # special flag used only when building the UI
    ENABLE_DEBUG_KEYS=false # no point in setting debug keys if we didn't install
    shift # past argument
    ;;

    --depend|--depends) # remake the dependencies before building
    MAKEDEPEND=true
    shift # past argument
    ;;

    --enable_debug_keys) # set --enableDebugKeyBindings to true in scripts/run_comp, will also trigger build/install
    ENABLE_DEBUG_KEYS=true
    shift # past argument
    ;;

    --just_debug_keys) # set --enableDebugKeyBindings to true in scripts/run_comp, will NOT trigger build/install
    ENABLE_DEBUG_KEYS=true
    INSTALL=false
    BUILD=false
    HOMESPACE="agnostic" # doesn't use the homespace and doesn't build, so... 
    shift # past argument
    ;;
    --clean|--spotless) #make spotless before building
    CLEANFIRST=true
    shift # past argument
    ;;

    --just_rpm) # Just build an rpm
    INSTALL=false
    BUILD=false
    BUILDRPM=true
    shift # past argument
    ;;

    --just_tar) # Just build a full release tar
    INSTALL=false
    BUILD=false
    BUILDTAR=true
    shift # past argument
    ;;

    --rpm) # Also build an rpm
    BUILDRPM=true
    shift # past argument
    ;;

    --rpm_install)
    INSTALL=false
    BUILD=false
    ONLYINSTALLRPM=true
    shift # past argument
    ;;

    --ui) # build and install just the UI
    INSTALL=false
    BUILD=false
    UIINSTALL=true # special flag used only when building the UI
    BUILDUI=true
    shift # past argument
    ;;

    --uidevel) # build and install just the UI with the develop flag set (disables polling, but builds faster and doesn't compress the code)
    INSTALL=false
    UIINSTALL=true # special flag used only when building the UI
    BUILD=false
    BUILDUIDEVEL=true
    shift # past argument
    ;;

    --noUIRefresh) # Don't try to send F5 to the app window
    REFRESHUI=false
    shift # past argument
    ;;

    --quiet) # Don't print stuff other than the spinners
    QUIET=true
    shift # past argument
    ;;

    --notify) # Send pushbullet notifications on finish/error
    MOBILEALERT=true
    shift # past argument
    ;;

    --just_notify) # Just send a pushbullet notification
    MOBILEALERT=true
    echo "--just_notify was set and the message was $2"
    BUILD=false
    INSTALL=false
    FUN_MESSAGE=$2
    HOMESPACE="agnostic" # doesn't use the homespace and doesn't build, so... 
    shift # past argument
    shift # past value
    ;;

    --build_comp|--build_compositor) # Build compositor as debug and install it to $INSTALL_DIR/
    BUILD=false
    BUILDSPECIFIC=true
    BUILDSPECIFICTYPE="debug"
    BUILDSPECIFICNAME["$NUM_SPECIFIC_BUILD"]="compositorApp"
    let NUM_SPECIFIC_BUILD=NUM_SPECIFIC_BUILD+1
    shift # past argument
    ;;

    --build_script|--build_scriptApp) # Build scriptApp as debug and install it to $INSTALL_DIR/
    BUILD=false
    BUILDSPECIFIC=true
    BUILDSPECIFICTYPE="debug"
    BUILDSPECIFICNAME["$NUM_SPECIFIC_BUILD"]="scriptApp"
    let NUM_SPECIFIC_BUILD=NUM_SPECIFIC_BUILD+1
    shift # past argument
    ;;

    --build_thing|--buildThing|--build_debug|--buildDebug) # Build a specified app as debug and install it to $INSTALL_DIR/
    BUILD=false
    BUILDSPECIFIC=true
    BUILDSPECIFICTYPE="debug"
    BUILDSPECIFICNAME["$NUM_SPECIFIC_BUILD"]="$2"
    let NUM_SPECIFIC_BUILD=NUM_SPECIFIC_BUILD+1
    shift # past argument
    shift # past value
    ;;

    --build_release|--buildRelease) # Build a specified app as release and install it to $INSTALL_DIR/
    BUILD=false
    BUILDSPECIFIC=true
    BUILDSPECIFICTYPE="release/bin"
    BUILDSPECIFICNAME["$NUM_SPECIFIC_BUILD"]="$2"
    let NUM_SPECIFIC_BUILD=NUM_SPECIFIC_BUILD+1
    shift # past argument
    shift # past value
    ;;

    --no_run_reserve) # Don't preserve changes to $INSTALL_DIR/run_comp
    FORCE_REFRESH=true
    shift # past argument
    ;;

    --purge_run_scripts) # Just go and reset $INSTALL_DIR/run_comp
    PURGE_RUN=true
    INSTALL=false
    BUILD=false
    shift # past argument
    ;;

    # --remote_user)
    # REMOTEUSER="$2"
    # shift # past argument
    # shift # past value
    # ;;

    # --remote_host)
    # BUILDREMOTE=true
    # REMOTE_TARGET="$2"
    # shift # past argument
    # shift # past value
    # ;;
    
    --remote_target)
    INSTALL_REMOTE=true
    REMOTE_TARGET="$2"
    shift # past argument
    shift # past value
    ;;
    
    --remote_noStart)
    REMOTE_NOSTART=true
    shift # past argument
    ;;
    
    --remote_reboot)
    REMOTE_NOSTART=true
    REMOTE_REBOOT=true
    shift # past argument
    ;;

    *)    # unknown option
    echo "Unknown operation: $1"
    echo "Valid operations are:"
    printHelp
    exit 1;
    ;;
esac
done

spinner()
{
    local pid=$!
    local delay=0.75
    local spinstr='.'

    if [ -z "$1" ] ; then 
        echo -n "Loading "
    else
        echo -n "$1"
    fi

    while [ "$(ps a | awk '{print $1}' | grep $pid)" ]; do
        printf "%s" "$spinstr"
        sleep $delay
    done
    printf "\n"
}

if [ "${HOMESPACE}" = false ] ; then
    echo "Must specify whether to build in --OTHER or --METRO";
    exit 1;
fi

#HOMESPACE being agnostic means there should be no actions done in a workspace.
if [ "${HOMESPACE}" != "agnostic" ] ; then
    WORKSPACE="$HOMESPACE""/build/metropolis/image"
    RELEASESPACE="$HOMESPACE""/build/metropolis/image/release"
    RPMWORKSPACE="$HOMESPACE""/build/metropolis/rpmbuild"
    UIWORKSPACE="$HOMESPACE""/source/ui_ember"

    cd "$WORKSPACE"

    if [ "${CLEANFIRST}" = true ] ; then
        echo -e "Making Spotless... in $HOMESPACE\n"
        sleep 2s
        make spotless &>/dev/null
        # spinner "Making Spotless"
    fi

    if [ "${MAKEDEPEND}" = true ] ; then
        echo -e "Making Dependencies... in $HOMESPACE\n"
        sleep 2s
        make -j8 depend
    fi

    if [ "${BUILDSPECIFIC}" = true ] ; then
        if [ "${BUILDSPECIFICNAME}" = false ] ; then
            echo "Woah, how did you get here? BUILDSPECIFICNAME is false."
            exit 1;
        fi
        echo "Going to build: ${BUILDSPECIFICNAME[@]}"
        for APP in "${BUILDSPECIFICNAME[@]}"; do
            echo -e "\nBuilding $BUILDSPECIFICTYPE/$APP... in $HOMESPACE\n"
            sleep 2s
            make $BUILDSPECIFICTYPE/$APP

            # Incase you didn't want to install it, you can still use the
            if [ "${INSTALL}" = true ] ; then
                if [ "${INSTALL_REMOTE}" = true ] ; then
                    if [ "${REMOTE_TARGET}" = false ] ; then
                        echo "Remote Target not specified, specify now?"
                        read REMOTE_TARGET
                    fi
                    echo -e "Going to install $BUILDSPECIFICTYPE/$APP \t into $REMOTE_TARGET:$INSTALL_DIR/bin/$APP"
                    
                    ping -c 1 $REMOTE_TARGET # make sure $REMOTE_TARGET is actually there
                    if [$? = 0] ; then 
                        echo -e "Couldn't find remote target: \"$REMOTE_TARGET\""
                        continue;
                    else
                        sleep 2s
                        set -x;
                        ssh ${REMOTEUSER}@$REMOTE_TARGET 'systemctl stop xlogin@metro'
                        ssh ${REMOTESUPER}@$REMOTE_TARGET "rm $INSTALL_DIR/bin/$APP"
                        scp "$BUILDSPECIFICTYPE/$APP" ${REMOTESUPER}@$REMOTE_TARGET:"$INSTALL_DIR/bin/$APP"
                        if [ "${REMOTE_NOSTART}" = false ] ; then
                            ssh ${REMOTEUSER}@$REMOTE_TARGET 'systemctl start xlogin@metro';
                        elif [ "$REMOTE_REBOOT" = true ] ; then
                            ssh ${REMOTEUSER}@$REMOTE_TARGET 'reboot';
                        fi
                        
                        set +x
                    fi
                    
                else
                    echo -e "Going to install $BUILDSPECIFICTYPE/$APP \n\t into $INSTALL_DIR/bin/$APP"
                    echo $MASTERBLASTER | "$THE_REAL_SLIM_SUDO" -S cp $BUILDSPECIFICTYPE/$APP "$INSTALL_DIR"/bin/$APP
                fi
            fi
            # spinner "Building"
        done

        if [ "${INSTALL}" = true ] ; then
            INSTALL=false # skip the install step below
        fi
        
    fi

    if [ "${BUILD}" = true ] ; then
        echo -e "Building... in $HOMESPACE\n"
        sleep 2s
        make -j8 all # There -j8 HAPPY DUNCAN?
        # spinner "Building"
    fi

    if [ "${BUILDRPM}" = true ] ; then
        cd "$RPMWORKSPACE"
        echo -e "Building RPM... in $RPMWORKSPACE\n"
        sleep 2s
        make -j8 all
        if [ "${INSTALL_REMOTE}" = true ]; then
            if [ "${REMOTE_TARGET}" = false ] ; then
                echo "Remote Target not specified, specify now?"
                read REMOTE_TARGET
            fi
            
            echo -e "Going to install `ls "$RPMWORKSPACE"/RPMS/x86_64/prism-sw*` \n\t into $REMOTE_TARGET"
            
            ping -c 1 $REMOTE_TARGET # make sure $REMOTE_TARGET is actually there
            if [$? -eq 0] ; then 
                echo -e "Couldn't find remote target: \"$REMOTE_TARGET\""
            else
                sleep 2s # Give some time to read the echo.
                set -x;
                ssh ${REMOTEUSER}@$REMOTE_TARGET 'systemctl stop xlogin@metro'
                ssh ${REMOTEUSER}@$REMOTE_TARGET 'rm -f /home/metro/prism-sw-*.rpm'
                scp "$RPMWORKSPACE"/RPMS/x86_64/prism-sw* ${REMOTEUSER}@$REMOTE_TARGET:~
                ssh ${REMOTEUSER}@$REMOTE_TARGET 'sudo yum install /home/metro/prism-sw* -y || sudo yum downgrade /home/metro/prism-sw* -y ';
                if [ "${REMOTE_NOSTART}" = false ] ; then
                    ssh ${REMOTEUSER}@$REMOTE_TARGET 'systemctl start xlogin@metro';
                elif [ "$REMOTE_REBOOT" = true ] ; then
                    ssh ${REMOTEUSER}@$REMOTE_TARGET 'reboot';
                fi
                set +x;
            fi
        fi
    elif [ "${BUILDTAR}" = true ] ; then
        cd "$WORKSPACE"
        echo -e "Building app and release tarball... in $WORKSPACE\n"
        sleep 2s
        make -j8 tar-release DESTDIR=fullDist
    fi

    if [ "${ONLYINSTALLRPM}" = true ] ; then
        cd "$RPMWORKSPACE"
        echo -e "Installing RPM... from $RPMWORKSPACE\n"
        sleep 2s
        if [ "${INSTALL_REMOTE}" = true ]; then
            if [ "${REMOTE_TARGET}" = false ] ; then
                echo "Remote Target not specified, specify now?"
                read REMOTE_TARGET
            fi
            
            echo -e "Going to install `ls "$RPMWORKSPACE"/RPMS/x86_64/prism-sw*` \n\t into $REMOTE_TARGET"
            
            ping -c 1 $REMOTE_TARGET # make sure $REMOTE_TARGET is actually there
            if [$? -eq 0] ; then 
                echo -e "Couldn't find remote target: \"$REMOTE_TARGET\""
            else
                sleep 2s # Give some time to read the echo.
                set -x;
                ssh ${REMOTEUSER}@$REMOTE_TARGET 'systemctl stop xlogin@metro'
                ssh ${REMOTEUSER}@$REMOTE_TARGET 'rm -f /home/metro/prism-sw-*.rpm'
                scp "$RPMWORKSPACE"/RPMS/x86_64/prism-sw* ${REMOTEUSER}@$REMOTE_TARGET:~
                ssh ${REMOTEUSER}@$REMOTE_TARGET 'sudo yum install /home/metro/prism-sw* -y || sudo yum downgrade /home/metro/prism-sw* -y ';
                if [ "${REMOTE_NOSTART}" = false ] ; then
                    ssh ${REMOTEUSER}@$REMOTE_TARGET 'systemctl start xlogin@metro';
                elif [ "$REMOTE_REBOOT" = true ] ; then
                    ssh ${REMOTEUSER}@$REMOTE_TARGET 'reboot';
                fi
                # set +x;
            fi
        fi
    fi
    set -x;

    # echo $MASTERBLASTER | "$THE_REAL_SLIM_SUDO" -S ls &>/dev/null

    if [ "${INSTALL}" = true ] ; then
        cd "$WORKSPACE" #Go back to the main workspace just in case.
        # echo -e "Installing... in $HOMESPACE\n"
        set -e
        # sleep 2s
        # EXISTENCE=false

        if [ "${INSTALL_REMOTE}" = true ]; then
            if [ "${REMOTE_TARGET}" = false ] ; then
                echo "Remote Target not specified, specify now?"
                read REMOTE_TARGET
            fi
            
            echo -e "Going to install "$RELEASESPACE" \n\t into $REMOTE_TARGET"
            
            # ping -c 1 $REMOTE_TARGET # make sure $REMOTE_TARGET is actually there
            # if [$? -eq 0] ; then 
            #     echo -e "Couldn't find remote target: \"$REMOTE_TARGET\""
            # else
            sleep 2s # Give some time to read the echo.
            set -x;
            ssh ${REMOTEUSER}@$REMOTE_TARGET 'systemctl stop xlogin@metro'
            rsync -aI $RELEASESPACE/* ${REMOTEUSER}@$REMOTE_TARGET:$INSTALL_DIR
            if [ "${REMOTE_NOSTART}" = false ] ; then
                ssh ${REMOTEUSER}@$REMOTE_TARGET 'systemctl start xlogin@metro';
            elif [ "$REMOTE_REBOOT" = true ] ; then
                ssh ${REMOTEUSER}@$REMOTE_TARGET 'reboot';
            fi
                # set +x;
            # fi
        fi

    fi

    if [ "${BUILDUI}" = true ] || [ "${BUILDUIDEVEL}" = true ] ; then
        cd "$UIWORKSPACE"

        if [ "${BUILDUI}" = true ]  ; then
            echo -e "Building UI... in $UIWORKSPACE\n"
            ember build --environment=production
        elif [ "${BUILDUIDEVEL}" = true ]  ; then
            echo -e "Building UI Devel... in $UIWORKSPACE\n"
            ember build --environment=development
        fi

        if [ "${UIINSTALL}" = true ] ; then
            # Install the new UI stuff
            
            if [ "${INSTALL_REMOTE}" = true ]; then
                if [ "${REMOTE_TARGET}" = false ] ; then
                    echo "Remote Target not specified, specify now?"
                    read REMOTE_TARGET
                fi
                
                echo -e "Going to install $UIWORKSPACE/dist \n\t into $REMOTE_TARGET:$INSTALL_DIR/webfiles"
                
                ping -c 1 $REMOTE_TARGET # make sure $REMOTE_TARGET is actually there
                if [$? -eq 0] ; then 
                    echo -e "Couldn't find remote target: \"$REMOTE_TARGET\""
                else
                    sleep 2s # Give some time to read the echo.
                    set -x;
                    ssh ${REMOTEUSER}@$REMOTE_TARGET 'systemctl stop xlogin@metro'
                    ssh ${REMOTESUPER}@$REMOTE_TARGET "rm -rf $INSTALL_DIR/webfiles"
                    rsync -aI "$UIWORKSPACE"/dist/* ${REMOTESUPER}@$REMOTE_TARGET:$INSTALL_DIR/webfiles
                    if [ "${REMOTE_NOSTART}" = false ] ; then
                        ssh ${REMOTEUSER}@$REMOTE_TARGET 'systemctl start xlogin@metro';
                    elif [ "$REMOTE_REBOOT" = true ] ; then
                        ssh ${REMOTEUSER}@$REMOTE_TARGET 'reboot';
                    fi
                    set +x;
                fi
            else
                "$THE_REAL_SLIM_SUDO" rm -rf "$INSTALL_DIR"/webfiles
                echo "Yeah I installed the UI stuff..."
                "$THE_REAL_SLIM_SUDO" mv "$UIWORKSPACE"/dist "$INSTALL_DIR"/webfiles
                

                if [ "${REFRESHUI}" = true ] ; then
                    # Super dumb, but what if it also refreshed the app...
                    WINDOWID=`xwininfo -root -tree | grep PRISM | xargs | cut -f 1 -d " "`
                    WINDOWNAME=`xwininfo -root -tree | grep PRISM | xargs | cut -d: -f1 | cut -d' ' -f3-`
                    if [ ! -z "$WINDOWID" ]; then
                        echo "Refreshing ${WINDOWID}-${WINDOWNAME}"
                        xdotool key --window "$WINDOWID" F5
                    fi
                fi
            fi
        fi
    fi

    if [ "${PURGE_RUN}" = true ] ; then
        # ( set -x; "$THE_REAL_SLIM_SUDO" cp "$WORKSPACE"/release/run                  "$INSTALL_DIR"/run )
        ( set -x; "$THE_REAL_SLIM_SUDO" rm "$INSTALL_DIR"/scripts/run_comp )
        ( set -x; "$THE_REAL_SLIM_SUDO" cp "$WORKSPACE"/release/scripts/run_comp "$INSTALL_DIR"/scripts/run_comp )
        echo "Installed run_comp script have been reset."
    fi

fi #End of workspace dependency

################################# workspace independent flags should go here ##############################
# if [ "${ENABLE_DEBUG_KEYS}" = true ] ; then
#     ( set -x; "$THE_REAL_SLIM_SUDO" sed -i -e 's/--enableDebugKeyBindings false/--enableDebugKeyBindings true/g' "$INSTALL_DIR"/scripts/run_comp )
# fi

if [ "${MOBILEALERT}" = true ] ; then
    if [ "${FUN_MESSAGE}" != false ] ; then
        pushMyBullet "${FUN_MESSAGE}" "Fun message from `hostname`";
    else
        pushMyBullet "`basename $0` has finished";
    fi
    
fi

# Notes:
# Never get involved in a land war in Asia.
# Node server.js location:
# /home/metro/workspace/metropolis_sw/packages/nodeServer/server.js
# export PATH=/usr/local/sbin:/usr/bin/:/usr/local/bin:/usr/sbin:/home/metro/scripts:/home/metro/.local/bin:/home/metro/bin

# Sort branches by date
# git for-each-ref --sort='-committerdate' --format='%(committerdate)%09%(refname)' refs/heads
#
# zone is "prism" not "public" because of snowflakes
#  
# #Forward port 9556 and reload
# "$THE_REAL_SLIM_SUDO" firewall-cmd --zone=prism --add-port=9556/tcp
# "$THE_REAL_SLIM_SUDO" firewall-cmd --reload
# "$THE_REAL_SLIM_SUDO" firewall-cmd --zone=prism --list-ports
# 
# #Forward external port 9556 to localhost:9555. Cef only allows connections from localhost
# ssh -L 0.0.0.0:9556:localhost:9555 localhost -N & #This has the problem that it needs authentication, but backgrounding it hides the fact.
# do the ssh without the '&', then ctrl-z to suspend, then `bg` to resume it in the background
#
# #Connect via [IP Address]:9556

# hippogriff
# reset TG: telnet in and run "/usr/local/bin/resetSystem"

# export vblank_mode=0 #add this line to run_comp to unlimit framerate.
# git merge -s recursive -Xpatience metropolis_develop

# sudo chown root chrome-sandbox; sudo chmod 4755 chrome-sandbox

#git config --global credential.helper store


# Using test station 
# scp RPM file to vpl-bulbasaur, and from there to the DUT of your choice.
# ssh metro@vpl-bulbasaur
# run pytest on vpl-bulbasaur (probably ~/goran/workspace/metropolis_sw, don't forget to check out the correct branch)

# Use below to install without rpm
# rsync -rplE release/ root@hostname.telestream.net:/usr/local/metropolis/

#change hostname command
# hostnamectl set-hostname <hostname Name>


#Dumb Locale issues.
# Use `locale` to check locale environment variables
# Use `locale -a | less` to get the list of available locales
# export LANG=<An available locale>
# export LC_ALL=<An available locale>

# [7/1 5:09 PM] Scott A. Johnson
#     rsync -rplE release/ root@hostname.telestream.net:/usr/local/metropolis/


#SSH Keys not working:
# https://serverfault.com/questions/321534/public-key-authentication-fails-only-when-sshd-is-daemon
# https://chemicloud.com/kb/article/ssh-authentication-refused-bad-ownership-or-modes-for-directory/
# TLDR: Check permissions/owner for "/home/$USER" and "/home/$USER/.ssh"
# They should not have write permissions for group or other users
# chmod go-w /home/metro # this will remove write permissions for group and other users

#SSH debugging:
# sudo /usr/sbin/sshd -d -p 2222 # Run on server side, starts SSH with debug on port 2222 (still need to open port 2222)
# sudo firewall-cmd --zone=prism --add-port=2222/tcp # Run on server side to open port 2222
# ssh -p 2222 user@hostname # ssh to port 2222, check terminal on server to see debug prints.

# Adding deb file to mirror: 
#   Copy new deb to rsync -aP <NEW_DEB_FILE> rion.wilson-yue@telestream.net@vpl-ubuntu-mirror:/opt/ubuntu_local_repo_data/pkg_releases/focal
#   ssh rion.wilson-yue@telestream.net@vpl-ubuntu-mirror
#   ^ should be network password
#   On vpl-ubuntu-mirror `cd /var/www/html/prism`
#   `sudo reprepro includedeb <release (focal/jammy)> /opt/ubuntu_local_repo_data/pkg_releases/focal/<NEW_DEB_FILE>`

#   To remove a package:
#   On vpl-ubuntu-mirror `cd /var/www/html/prism`
#   `sudo reprepro remove <release> <package name, not deb file name>`


# auto top batching
#top -bc -o COMMAND -p $(pgrep -d',' -f CEFSubProc)
# -b is batch mode
# -c lists the full command (not just executable name)
# -o sorts by column (COMMAND in this case)
# -p only looks at the following PIDs (expects comma seperated list)
# pgrep to look up the PIDs
# -d sets delimiter for top
# -f is the command to search for

# Access to other build nodes vpl-sabre and JenkinsBuild2 - user: "Jenkins", password: `old metro password`