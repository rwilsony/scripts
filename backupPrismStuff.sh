#!/bin/bash

theHOST=""
OPTIONAL_IP=false
IS_RESTORE=false
IS_BACKUP=false
RENAME_FOLDERS=false
POSITIONAL=()
BACKUP_FOLDER="/home/$USER/PrismBakups"
REMOTE_USER="metro"
REMOTE_ROOT="root"
REMOTE_HOME="/home/$REMOTE_USER"
REBOOT_REMOTE=true

bold=$(tput bold)
normal=$(tput sgr0)

printHelp() {
    echo "Backs up PRISM files & hostname to $BACKUP_FOLDER/<HOSTNAME>"
    echo ""
    echo "Backup (Before pave): "
    echo "    ${bold}backupPrismStuff.sh --backup --remote_target <HOSTNAME> --ip <IP_ADDRESS (optional)>${normal}"
    echo ""
    echo "Restore (After pave, after restoring DHCP/IP):"
    echo "Will set hostname to <HOSTNAME>. THIS WILL ALSO REBOOT THE PRISM UNLESS \"--no-reboot\" IS SPECIFIED): "
    echo "    ${bold}backupPrismStuff.sh --restore --remote_target <HOSTNAME> --ip <IP_ADDRESS (optional if HOSTNAME is set)>${normal}"
    echo ""
    echo "--help                                Print this stuff."
    echo "-t|--remote_target                    Hostname to backup."
    echo "--ip                                  Optional IP Address (For restoring before the box has a hostname)."
    echo "-r|--restore                          Copy the saved files back to the box."
    echo "-b|--backup                           Save the important files from the box."
    echo "--no-reboot                           Don't reboot the PRISM after restoring files."
    # echo "--rename                              Rename a folder. Why is this here again?"
}

while [[ $# -gt 0 ]]; do
    key="$1"

    case $key in
    -t | --remote_target)
        theHOST="$2"
        shift # past argument
        shift # past value
        ;;

    -r | --restore) #Specify a pid to snapshot the pmap of.
        IS_RESTORE=true
        shift # past value
        ;;

    -b | --backup) #Specify a pid to snapshot the pmap of.
        IS_BACKUP=true
        shift # past value
        ;;

    --rename)
        RENAME_FOLDERS=true
        shift
        ;;

    --no-reboot)
        REBOOT_REMOTE=false
        shift
        ;;

    --ip)
        OPTIONAL_IP="$2"
        shift # past argument
        shift # past value
        ;;

    -h | --help)
        printHelp
        exit 0
        ;;

    *) # unknown option
        POSITIONAL+=("$1")
        shift # past argument
        ;;
    esac
done

BACKUP_LOC="${BACKUP_FOLDER}/${theHOST}/"


if [ "${RENAME_FOLDERS}" = true ]; then
    cd ${BACKUP_FOLDER}
    for dutBackup in $(find . \! -name . -prune -type d); do
        if test -f "$dutBackup/remoteHostname"; then
            bemoteHost="./$(cat "$dutBackup"/remoteHostname)"
            if [ $dutBackup != "$bemoteHost" ]; then
                set -x
                ssh-keygen -f "~/.ssh/known_hosts" -R "$dutBackup"
                set +x
                echo "Renaming $dutBackup to $bemoteHost"
                mv $dutBackup $bemoteHost
            fi
        fi
    done
fi

if [ "${OPTIONAL_IP}" != false ]; then
    echo "OPTIONAL_IP: $OPTIONAL_IP"
    ssh-keygen -f "/home/$USER/.ssh/known_hosts" -R "$OPTIONAL_IP"
    theHOST="$OPTIONAL_IP"
fi

if [ "${IS_BACKUP}" = true ]; then
    set -x
    set -e
    ssh-copy-id metro@"$theHOST"
    #    ssh-copy-id root@"$theHOST"
    mkdir -p "$BACKUP_LOC"
    echo $(ssh metro@$theHOST "echo \$HOSTNAME") >"$BACKUP_LOC"/remoteHostname
    rsync -aI --ignore-missing-args metro@"$theHOST":/home/metro/serialNumber   "$BACKUP_LOC"
    rsync -aI --ignore-missing-args metro@"$theHOST":/home/metro/nomenclature   "$BACKUP_LOC"
    rsync -aI --ignore-missing-args metro@"$theHOST":/home/metro/.bashrc        "$BACKUP_LOC"
    rsync -aI --ignore-missing-args metro@"$theHOST":/home/metro/.bash_history  "$BACKUP_LOC"
    rsync -aI --ignore-missing-args metro@"$theHOST":/home/metro/.inputrc       "$BACKUP_LOC"
    rsync -a  --ignore-missing-args metro@"$theHOST":/home/metro/presets        "$BACKUP_LOC"
    rsync -a  --ignore-missing-args metro@"$theHOST":/home/metro/licenses       "$BACKUP_LOC"
    rsync -a  --ignore-missing-args metro@"$theHOST":/home/metro/options        "$BACKUP_LOC"
    rsync -a  --ignore-missing-args metro@"$theHOST":/home/metro/cache          "$BACKUP_LOC"
    # Pull the Ubuntu conversion/upgrade logs, just in case...
    rsync -a  --ignore-missing-args metro@"$theHOST":/tmp/.conversionStuff      "$BACKUP_LOC"/Upgrade_Logs --exclude="*.sqfs"
    rsync -a  --ignore-missing-args metro@"$theHOST":/var/tmp/.conversionStuff  "$BACKUP_LOC"/Upgrade_Logs --exclude="*.sqfs"
    rsync -a  --ignore-missing-args metro@"$theHOST":/var/log/installer         "$BACKUP_LOC"/Paver_Logs --exclude="*.yaml" --exclude="curtin-install.log"
        # Has protected yamls and log, so skip 'em
    set +x

elif [ "${IS_RESTORE}" = true ] && [ "$(ls -A $BACKUP_LOC)" ]; then
    set -x
    ssh-keygen -f "/home/$USER/.ssh/known_hosts" -R "$theHOST"

    if test -f "$BACKUP_LOC/remoteHostname"; then
        bemoteHost=$(cat "$BACKUP_LOC"/remoteHostname)
    else
        bemoteHost=$theHOST
    fi

    ssh-copy-id metro@"$theHOST"
    ssh-copy-id root@"$theHOST"
    ssh metro@"$theHOST" "systemctl stop xlogin@metro"

    ssh root@"$theHOST" "rm -rf $REMOTE_HOME/presets/"
    rsync -aI --ignore-missing-args "$BACKUP_LOC"/serialNumber  metro@"$theHOST":/home/metro/serialNumber
    rsync -aI --ignore-missing-args "$BACKUP_LOC"/nomenclature  metro@"$theHOST":/home/metro/nomenclature
    rsync -aI --ignore-missing-args "$BACKUP_LOC"/.bashrc       metro@"$theHOST":/home/metro/.bashrc
    rsync -aI --ignore-missing-args "$BACKUP_LOC"/.bash_history metro@"$theHOST":/home/metro/.bash_history
    rsync -aI --ignore-missing-args "$BACKUP_LOC"/.inputrc      metro@"$theHOST":/home/metro/.inputrc
    rsync -aI --ignore-missing-args "$BACKUP_LOC"/presets/      metro@"$theHOST":/home/metro/presets
    rsync -aI --ignore-missing-args "$BACKUP_LOC"/licenses/     metro@"$theHOST":/home/metro/licenses
    rsync -aI --ignore-missing-args "$BACKUP_LOC"/cache/        metro@"$theHOST":/home/metro/cache
    # rsync -aI --ignore-missing-args "$BACKUP_LOC"/options/      metro@"$theHOST":/home/metro/options

    if command -v yum  &> /dev/null; then
        # If yum is present then we're on CentOS
        ssh metro@"$theHOST" "sudo yum install htop tmux vim vpl-libcef-devel -y"
        ssh root@"$theHOST" "usermod -a -G wheel metro"
    fi

    ssh root@"$theHOST" "hostnamectl set-hostname $bemoteHost"
    set +x

    if [ $REBOOT_REMOTE = true ]; then
        echo "Rebooting..."
        sleep 2s
        ssh root@"$theHOST" "reboot"
    else 
        echo "Not Rebooting!"
    fi
    # Set hostname using `hostnamectl set-hostname`

fi

# Enabling DHCP from cli
# nmtui; go to "Activate a connection"
