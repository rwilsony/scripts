/*
 * Something about testing pulseaudio API
 */

#include <stdio.h>
#include <string.h>
#include <pulse/pulseaudio.h>
#include <vector>
#include <unistd.h>
#include <iostream>
#include <time.h>
#include <cstdlib>

int doThing();
void pulseStateChangeCallBack( pa_context *context, void *userdata );
void pulseListSinksCallBack( pa_context *c, const pa_sink_info *sinkInfo, int eol, void *userdata );
void pulseListSourcesCallBack( pa_context *c, const pa_source_info *sourceInfo, int eol, void *userdata );
void pulseAudioSinkEventSuccessCB( pa_context *c,
                                   pa_subscription_event_type_t eventType,
                                   uint32_t index,
                                   void *userdata );
void addSinkCallBack( pa_context *context, const pa_sink_info *sinkInfo, int eol, void *userdata );
void removeSink( uint32_t index );
void volumeChangeCallback( pa_context *c, int success, void *userdata );

float paVol_Linear_to_Percent( pa_volume_t linearVolume )
{
    return ( linearVolume / float( PA_VOLUME_NORM ) ) * 100.0f;
};

// Member variables
pa_mainloop *m_paMainLoop;
pa_mainloop_api *m_paMainLoopAPI;
pa_context *m_paContext;
pa_operation *m_paOperationStatus;

struct tekPulseAudioStruct
{
    std::string name;        /**< Name of sink */
    std::string description; /**< Pulse Audio's sink description */
    uint32_t index = -1;     /**< Pulse Audio sink index */
    int mute       = -1;     /**< mute */
    pa_cvolume volumes;

    tekPulseAudioStruct() = default;

    // These two down here are make up the pa_cvolume struct
    // int numChannels;                   /**< Number of channels */
    // uint32_t volumes[PA_CHANNELS_MAX]; /**< CONTAINS ALL CHANNEL'S VOLUMES WAKKA */
    tekPulseAudioStruct &operator=( const pa_sink_info &sinkInfoRightSide )
    {
        name             = std::string( sinkInfoRightSide.name );
        description      = std::string( sinkInfoRightSide.description );
        index            = int( sinkInfoRightSide.index );
        mute             = int( sinkInfoRightSide.mute );
        volumes.channels = int( sinkInfoRightSide.volume.channels );
        memcpy( volumes.values, sinkInfoRightSide.volume.values, PA_CHANNELS_MAX );
        return *this;
    };

    tekPulseAudioStruct( const pa_sink_info &sinkInfoRightSide )
    {
        name             = std::string( sinkInfoRightSide.name );
        description      = std::string( sinkInfoRightSide.description );
        index            = int( sinkInfoRightSide.index );
        mute             = int( sinkInfoRightSide.mute );
        volumes.channels = int( sinkInfoRightSide.volume.channels );
        memcpy( volumes.values, sinkInfoRightSide.volume.values, PA_CHANNELS_MAX );
    };
};

std::ostream &operator<<( std::ostream &out, const pa_cvolume &volumeStruct )
{
    out << "    numVolumeChannels:\t" << int( volumeStruct.channels ) << std::endl;
    for ( int i = 0; i < volumeStruct.channels; i++ )
    {
        out << "        Chan" << i << " Vol:\t\t";
        printf( "%.2f", paVol_Linear_to_Percent( volumeStruct.values[i] ) );
        out << "% (" << volumeStruct.values[i] << "/" << PA_VOLUME_NORM << ")" << std::endl;
    }
    return out;
}

std::ostream &operator<<( std::ostream &out, const tekPulseAudioStruct &struttingStruct )
{
    out << "tekPulseAudioStruct named: " << struttingStruct.name << std::endl;
    out << "    Description:\t\t" << struttingStruct.description << std::endl;
    out << "    Index:\t\t\t" << struttingStruct.index << std::endl;
    out << "    mute:\t\t\t" << struttingStruct.mute << std::endl;
    out << struttingStruct.volumes << std::endl;
    return out;
}

std::vector<pa_source_info> m_inputDevices; // This is where we'll store the input device list

std::vector<tekPulseAudioStruct> m_outputDevices; // This is where we'll store the output device list
int m_machinaState = 0;
int m_paReady      = 0;
bool runForrest    = true;
int m_dumbCountr   = 0;

// Main needs to set up (and act like periodic house keeping)
int main()
{
    m_paMainLoop    = pa_mainloop_new();                   // Defining a new work loop for the pulsAudio API
    m_paMainLoopAPI = pa_mainloop_get_api( m_paMainLoop ); // This is just to get the context, pa_context, below
    m_paContext =
        pa_context_new( m_paMainLoopAPI, "PA test ripoff" ); // Get a new context to connect to the Pulse Audio
                                                             // Server. Takes the API and an application name.

    // This is Funky. This will contain the result/state of the current requested operation.

    // This function connects to the pulse server
    pa_context_connect( m_paContext, NULL, PA_CONTEXT_NOFLAGS, NULL );

    // This function defines a callback so the server will tell us it's state.
    // Our callback will wait for the state to be ready.  The callback will
    // modify the variable to 1 so we know when we have a connection and it's
    // ready.
    // If there's an error, the callback will set m_paReady to 2
    // Sets up a callback for pulseAudio to call when its state changes.
    pa_context_set_state_callback( m_paContext, pulseStateChangeCallBack, NULL );

    while ( runForrest )
    {
        /*
            Member for initComplete
            doThing will be called until initComplete
            if ( !initComplete)
                doThing
            pa_mainloop_iterate

        */

        usleep( 100000 );
        if ( doThing() < 0 )
        {
            std::cout << "Something bad happened. Clean up should have already happened, quitting." << std::endl;
            pa_context_disconnect( m_paContext );
            pa_context_unref( m_paContext );
            pa_mainloop_free( m_paMainLoop );
            return 1;
        }
        else
        {
            pa_mainloop_iterate( m_paMainLoop, 0, NULL );
        }
    }

    // std::cout << "Input devices:" << std::endl;
    // for ( pa_source_info i : m_inputDevices )
    // {
    //     std::cout << i.name << " is at index: " << i.index << std::endl;
    //     std::cout<< "\t" << i.description << std::endl;
    // }
    // std::cout << "\n\nOutput devices:" << std::endl;

    // for ( pa_sink_info i : m_outputDevices )
    // {
    //     std::cout << i.name << " is at index: " << i.index << std::endl;
    //     std::cout << "\t" << i.description << std::endl;
    // }
    return 0;
}

int doThing()
{
    // static unsigned x = 0;
    // Didn't fail, but not ready.
    if ( m_paReady == 0 )
    {
        // std::cout << "Here in doThing: m_paReady is: " << m_paReady << std::endl;
        return 0;
    }
    else if ( m_paReady == 2 )
    {
        std::cout << "Shit. Cleaning." << std::endl;
        // Something bad happened or ther server is gone.
        // Pull the plug
        pa_context_disconnect( m_paContext );
        pa_context_unref( m_paContext );
        pa_mainloop_free( m_paMainLoop );
        return -1;
    }

    switch ( m_machinaState )
    {
        // State 0: we haven't done anything yet
        case 0:
            // Issues the request for the sink list.
            // m_paOperationStatus contains the current status
            // pulseListSinksCallBack is the callback that pa_mainloop will call
            // Third is userdata, but it's going to fill a member variable, so no need to pass it along.
            m_paOperationStatus = pa_context_get_sink_info_list( m_paContext, pulseListSinksCallBack, NULL );
            // printf( "Here in m_machinaState %d\n", m_machinaState );

            // Move on to next state.
            m_machinaState++;
            break;
        case 1:
            // Now we wait for our operation to complete.  When it's
            // complete our pa_output_devicelist is filled out, and we move
            // along to the next state
            if ( pa_operation_get_state( m_paOperationStatus ) == PA_OPERATION_DONE )
            {
                pa_operation_unref( m_paOperationStatus );
                // printf( "Here in m_machinaState %d\n", m_machinaState );

                // Now we perform another operation to get the source
                // (input device) list just like before.  This time we pass
                // a pointer to our input structure
                m_paOperationStatus = pa_context_get_source_info_list( m_paContext, pulseListSourcesCallBack, NULL );
                // Update the m_machinaState so we know what to do next
                m_machinaState++;
            }
            else
            {
                // printf( "Here in m_machinaState %d, waiting for previous to finish\n", m_machinaState );
                // std::cout << "Operation status is currently: " << pa_operation_get_state( m_paOperationStatus ) <<
                // std::endl;
            }
            break;
        case 2:
            if ( pa_operation_get_state( m_paOperationStatus ) == PA_OPERATION_DONE )
            {
                // Now we're done, clean up and disconnect and return
                pa_operation_unref( m_paOperationStatus );
                for ( tekPulseAudioStruct audStruct : m_outputDevices )
                {
                    std::cout << audStruct << std::endl;
                }

                // pa_context_disconnect( m_paContext );
                // pa_context_unref( m_paContext );
                // pa_mainloop_free( m_paMainLoop );
                // runForrest = false;
                m_machinaState++;
                break;
            }
            else
            {
                // printf( "Here in m_machinaState %d, waiting for previous to finish\n", m_machinaState );
            }
        case 3:
            // std::cout << "Waiting for new events" << std::string("...").substr((x%3)) << std::endl;
            // x++;
            if ( !(m_dumbCountr % 20) )
            {
                // m_dumbCountr = 0;
                // m_machinaState = 0;
                // std::cout << m_dumbCountr << std::endl;
                // for ( tekPulseAudioStruct audStruct : m_outputDevices )
                for ( unsigned i = 0; i < m_outputDevices.size(); i++ )
                    if ( m_outputDevices[i].name.find( "Jabra" ) != std::string::npos )
                    {
                        std::cout << "#######################################################################" << std::endl;
                        std::cout << "Jabra Current volume is: \n" << m_outputDevices[i].volumes << std::endl;
                        float newVolPercent = paVol_Linear_to_Percent( m_outputDevices[i].volumes.values[0] ) + ( rand() % 10 + 1 - 5 );
                        if ( newVolPercent > 80 || newVolPercent < 10 )
                            newVolPercent = 50;
                        uint32_t newVolLinear = ( newVolPercent / 100 ) * PA_VOLUME_NORM;
                        std::cout << "------------------" << std::endl;
                        std::cout << "Changing to: " << newVolPercent << "% (" << newVolLinear << "/" << PA_VOLUME_NORM
                                  << ")" << std::endl;
                        pa_cvolume_set( &m_outputDevices[i].volumes, m_outputDevices[i].volumes.channels, newVolLinear );
                        std::cout << "m_outputDevices[i].index: " << m_outputDevices[i].index << std::endl;
                        std::cout << "m_outputDevices[i].volumes: " << m_outputDevices[i].volumes << std::endl;
                        pa_context_set_sink_volume_by_index( m_paContext, m_outputDevices[i].index, &m_outputDevices[i].volumes,
                                                             volumeChangeCallback, NULL );
                    }
            }
            if( m_dumbCountr > 2000)
                return -1;


            m_dumbCountr++;
            break;
        default:
            // We should never see this m_machinaState
            fprintf( stderr, "in m_machinaState %d\n", m_machinaState );
            std::cout << "Shit. Cleaning." << std::endl;
            return -1;
    }
    return 0;
}

// This callback gets called when our context changes state.  We really only
// care about when it's ready or if it has failed
void pulseStateChangeCallBack( pa_context *context, void *userdata )
{
    pa_context_state_t state;
    state = pa_context_get_state( context );
    // std::cout << "State has changed to: " << state << std::endl;
    switch ( state )
    {
        // There are just here for reference
        case PA_CONTEXT_UNCONNECTED:
        case PA_CONTEXT_CONNECTING:
        case PA_CONTEXT_AUTHORIZING:
        case PA_CONTEXT_SETTING_NAME:
        default:
            break;
        case PA_CONTEXT_FAILED:
        case PA_CONTEXT_TERMINATED:
            m_paReady = 2;
            break;
        case PA_CONTEXT_READY:
            m_paReady = 1;
            // PA_SUBSCRIPTION_EVENT_SINK

            pa_context_subscribe( m_paContext, pa_subscription_mask_t( PA_SUBSCRIPTION_MASK_SINK ), NULL, NULL );

            pa_context_set_subscribe_callback( m_paContext, pulseAudioSinkEventSuccessCB, userdata );

            break;
    }
    // std::cout << "m_paReady: " << m_paReady << std::endl;
}

// This callback is called for EACH sink
void pulseListSinksCallBack( pa_context *context, const pa_sink_info *sinkInfo, int eol, void *userdata )
{
    // If eol is set to a positive number, you're at the end of the list
    // How the hell was I supposed to know that from the documentation?
    if ( eol > 0 )
    {
        // std::cout << "Here in pulseListSinksCallBack() eol is: " << eol << std::endl;
        return;
    }

    tekPulseAudioStruct tempAudioStruct = *sinkInfo;
    m_outputDevices.push_back( tempAudioStruct );
}

// This callback is called for EACH sink
void pulseListSourcesCallBack( pa_context *context, const pa_source_info *sourceInfo, int eol, void *userdata )
{
    if ( eol > 0 )
    {
        return;
    }

    pa_source_info newSource = *sourceInfo;
    m_inputDevices.push_back( newSource );
}

void pulseAudioSinkEventSuccessCB( pa_context *context,
                                   pa_subscription_event_type_t eventType,
                                   uint32_t index,
                                   void *userdata )
{
    // std::cout << "pulseAudioSinkEventSuccessCB() was called with index: " << index << std::endl;
    // std::cout << "\t\t type: " << std::hex << eventType << std::dec << std::endl;
    if ( ( eventType & PA_SUBSCRIPTION_EVENT_FACILITY_MASK ) == PA_SUBSCRIPTION_EVENT_SINK )
    {
        if ( ( eventType & PA_SUBSCRIPTION_EVENT_TYPE_MASK ) == PA_SUBSCRIPTION_EVENT_NEW )
        {
            std::cout << "pulseAudioSinkEventSuccessCB() was called because sink " << index << " was added"
                      << std::endl;
            pa_context_get_sink_info_by_index( context, index, addSinkCallBack, userdata );
        }
        else if ( ( eventType & PA_SUBSCRIPTION_EVENT_TYPE_MASK ) == PA_SUBSCRIPTION_EVENT_REMOVE )
        {
            std::cout << "pulseAudioSinkEventSuccessCB() was called because sink " << index << " was removed"
                      << std::endl;
            removeSink( index );
        }
    }
}

/*  addSinkCallBack - callback for pa_context_get_sink_info_by_index() to add a sink to m_outputDevices
 *
 *
 *
 */
void addSinkCallBack( pa_context *context, const pa_sink_info *sinkInfo, int eol, void *userdata )
{

    if ( eol > 0 ) // Terminating Call, quit.
        return;

    // MyClass * thePointerFormallyKnownAsThis = reinterpret_cast<MyClass*>(userdata);

    std::cout << "Currently " << m_outputDevices.size() << " items in m_outputDevices" << std::endl;
    std::cout << "Adding sink: " << sinkInfo->name << std::endl;
    tekPulseAudioStruct tempAudioStruct = *sinkInfo;
    m_outputDevices.push_back( tempAudioStruct );
    std::cout << "There are now " << m_outputDevices.size() << " items in m_outputDevices" << std::endl;
    for ( tekPulseAudioStruct audStruct : m_outputDevices )
    {
        std::cout << audStruct << std::endl;
    }
}

/*  removeSink - callback for pa_context_get_sink_info_by_index() to remove a sink from m_outputDevices
 *
 *
 *
 */
void removeSink( uint32_t index )
{
    // CAREFUL: sinkInfo doesn't exist because it was removed.
    std::cout << "Currently " << m_outputDevices.size() << " items in m_outputDevices" << std::endl;
    std::cout << "Removing sink at: " << index << std::endl;
    for ( unsigned i = 0; i < m_outputDevices.size(); i++ )
    {
        if ( m_outputDevices[i].index == index )
        {
            m_outputDevices.erase( m_outputDevices.begin() + i );
            break;
        }
    }
    std::cout << "There are now " << m_outputDevices.size() << " items in m_outputDevices" << std::endl;
    for ( tekPulseAudioStruct audStruct : m_outputDevices )
    {
        std::cout << audStruct << std::endl;
    }
}

/*  volumeChangeCallback
 *  callback
 *
 *
 */
void volumeChangeCallback( pa_context *c, int success, void *userdata )
{
    std::cout << "volumeChange was a success: " << success << "\n\n" << std::endl;
    // call status here?
}
